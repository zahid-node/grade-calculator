var book = require("./lib/grades").book;
const express = require('express');
const app = express();
const port = process.env.PORT || 3000;

app.get('/',(req,res) => {
	res.send('Hello World!...');
});

app.get('/grade',(req,res) => {
	// "1,2,3".split(",") => [1,2,3];
	var grades = req.query.grades.split(",");
	for(var i = 0; i <= grades.length; i++){
		book.addGrade(parseInt(grades[i]));
	}
	var average = book.getAverage();
	var letter = book.getLetterGrade();
	res.send("Your average is" +" " +  average + " " +  "grade" + "  " + letter);
});

app.listen(port,() => {
	console.log(`Server is up and running on port : ${port}`);
});